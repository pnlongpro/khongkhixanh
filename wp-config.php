<?php
define('WP_CACHE', true); // Added by WP Rocket
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'localhost_khongkhixanh');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

define('WP_MEMORY_LIMIT', '512M');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'x@UiIJ5JpIBVa)cploWLwv$d=CzG.IUo?-/t9HS+6C2aQwNB:Q#qj@56hA&T%-p ');
define('SECURE_AUTH_KEY',  'jG,&p Hu,Re{9AV*kjmaj[^^9C$Pvt`(af7x{du`3H^~FpzZ p!x=Ab5Kd,2V_Zh');
define('LOGGED_IN_KEY',    'N.8twgThdPc;M*-<QJ#7_&8]3[3?~R*Fj2~5V@5F?>T6:c#$9uj.IjhH?`xth|L0');
define('NONCE_KEY',        '%>}|7m%^/P{KX:3F/lnmRJ]jYZtZQt-a-:YSDS3Fk;-!}Y|>(Eu:vX#X>JH=Q!`c');
define('AUTH_SALT',        'mRuNyOc/zqp?5]>.R6l:Z=NtNvfPR!1!SS>#?HDhNn*XUxfSR4GR{8<Ns(Tk[8_[');
define('SECURE_AUTH_SALT', ';0>FCgNS3.v}Cp#*A47dpS__PWY,%!IW#xKp# ,KVbQ#-=kFp~C6BR;%/t$TL-<0');
define('LOGGED_IN_SALT',   '6U)2KmDBQCtc&Y*;VHDL+mhv1?xw{p|+esG@1>6x$B5V6M<Z6JMRDDFZu_X@~(ll');
define('NONCE_SALT',       'jx|k>{eLpyvk?spggeuWiLF1ysine~`N?|?wE#CieD~TondnUbnt[eFgOQXD>D}N');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'vtf_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');