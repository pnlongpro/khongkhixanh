<?php
/*
Plugin Name: Trả góp bằng thẻ tín dụng - Trung Hải
Description: Plugin trả góp bằng thẻ tin dụng
Author: Phan Long
Version: 1.0
Author URI: http://fb.com/pnlongpro/
*/
add_action( 'plugins_loaded', 'wc_offline_gateway_init', 11 );

function wc_offline_gateway_init() {

	class WC_Gateway_Offline extends WC_Payment_Gateway {

		public function __construct() {
			$this->id = 'trunghai-payment';
			$this->has_fields = false;
			$this->method_title = 'Trả góp bằng thẻ tín dụng';
			$this->method_description = 'Trả góp bằng thẻ tín dụng';
			$this->init_form_fields();
			$this->init_settings();
			add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );
			add_action( 'woocommerce_thankyou_alepay', array( $this, 'thankyou_page' ) );
		}
		public function process_admin_options() {
			return parent::process_admin_options();
		}

		public function init_form_fields() {

			$this->form_fields = apply_filters( 'wc_offline_form_fields', array(

				'enabled' => array(
					'title'   => __( 'Enable/Disable', 'trunghai-payment' ),
					'type'    => 'checkbox',
					'label'   => __( 'Enable Offline Payment', 'trunghai-payment' ),
					'default' => 'yes'
				),

				'title' => array(
					'title'       => __( 'Title', 'trunghai-payment' ),
					'type'        => 'text',
					'description' => __( 'Trả góp bằng thẻ tín dụng', 'trunghai-payment' ),
					'default'     => __( 'Offline Payment', 'trunghai-payment' ),
					'desc_tip'    => true,
				),

				'description' => array(
					'title'       => __( 'Description', 'trunghai-payment' ),
					'type'        => 'textarea',
					'description' => __( 'Payment method description that the customer will see on your checkout.', 'trunghai-payment' ),
					'default'     => __( 'Chúng tôi sẽ liên hệ và tư vấn cho bạn qua số điện thoại', 'trunghai-payment' ),
					'desc_tip'    => true,
				),

				'instructions' => array(
					'title'       => __( 'Instructions', 'trunghai-payment' ),
					'type'        => 'textarea',
					'description' => __( 'Instructions that will be added to the thank you page and emails.', 'trunghai-payment' ),
					'default'     => '',
					'desc_tip'    => true,
				),
			) );
			// Define user set variables
			$this->title = $this->get_option('title');
			$this->description = $this->get_option('description', 'Chọn một phương thức');
			$this->instructions = $this->get_option('instructions', $this->description);
			$this->order_status = $this->get_option('order_status');
			//$this->logging = 'yes' === $this->get_option( 'logging' );
			$this->payment_normal = 'yes' === $this->get_option('payment_normal');
			$this->payment_normal_desc = $this->get_option('payment_normal_desc');
			$this->payment_installment = 'yes' === $this->get_option('payment_installment');
			$this->payment_installment_desc = $this->get_option('payment_installment_desc');
			$this->payment_token = 'yes' === $this->get_option('payment_token');
			$this->payment_token_desc = $this->get_option('payment_token_desc');
			$this->language = $this->get_option('language', 'vi');
			$this->currency = $this->get_option('currency', 'VND');
		}
		public function get_option( $key, $empty_value = null ) {
			if ( empty( $this->settings ) ) {
				$this->init_settings();
			}

			// Get option default if unset.
			if ( ! isset( $this->settings[ $key ] ) ) {
				$form_fields            = $this->get_form_fields();
				$this->settings[ $key ] = isset( $form_fields[ $key ] ) ? $this->get_field_default( $form_fields[ $key ] ) : '';
			}

			if ( ! is_null( $empty_value ) && '' === $this->settings[ $key ] ) {
				$this->settings[ $key ] = $empty_value;
			}

			return $this->settings[ $key ];
		}
		public function process_payment( $order_id ) {

			$order = wc_get_order( $order_id );

			// Mark as on-hold (we're awaiting the payment)
			$order->update_status( 'on-hold', __( 'Awaiting offline payment', 'trunghai-payment' ) );

			// Reduce stock levels
			$order->reduce_order_stock();

			// Remove cart
			WC()->cart->empty_cart();

			// Return thankyou redirect
			return array(
				'result'    => 'success',
				'redirect'  => $this->get_return_url( $order )
			);
		}
		public function thankyou_page() {
			if ( $this->instructions ) {
				echo wpautop( wptexturize( $this->instructions ) );
			}
		}
		public function email_instructions( $order, $sent_to_admin, $plain_text = false ) {

			if ( $this->instructions && ! $sent_to_admin && 'offline' === $order->payment_method && $order->has_status( 'on-hold' ) ) {
				echo wpautop( wptexturize( $this->instructions ) ) . PHP_EOL;
			}
		}


	} // end \WC_Gateway_Offline class
}
function wc_offline_add_to_gateways( $gateways ) {
	$gateways[] = 'WC_Gateway_Offline';
	return $gateways;
}
add_filter( 'woocommerce_payment_gateways', 'wc_offline_add_to_gateways' );