<div class="row category-page-row">

	<div class="col large-3 hide-for-medium <?php flatsome_sidebar_classes(); ?>">
		<div id="shop-sidebar" class="sidebar-inner col-inner">
			<?php
			if ( is_active_sidebar( 'shop-sidebar' ) ) {
				dynamic_sidebar( 'shop-sidebar' );
			} else {
				echo '<p>You need to assign Widgets to <strong>"Shop Sidebar"</strong> in <a href="' . get_site_url() . '/wp-admin/widgets.php">Appearance > Widgets</a> to show anything here</p>';
			}
			?>
		</div><!-- .sidebar-inner -->
	</div><!-- #shop-sidebar -->

	<div class="col large-9">
		<?php
		/**
		 * woocommerce_before_main_content hook
		 *
		 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
		 * @hooked woocommerce_breadcrumb - 20
		 */
		do_action( 'woocommerce_before_main_content' );
		?>
		<?php
		$term               = get_queried_object();
		$parent_id          = empty( $term->term_id ) ? 0 : $term->term_id;
		$product_categories = get_categories( apply_filters( 'woocommerce_product_subcategories_args', array(
			'parent'       => $parent_id,
//			'menu_order'   => 'DESC',
			'hide_empty'   => 0,
			'hierarchical' => 1,
			'taxonomy'     => 'product_cat',
			'pad_counts'   => 1,
		) ) );
		//			var_dump($product_categories);
		if ( is_array( $product_categories ) && !empty( $product_categories ) ) :

			foreach( $product_categories as $key => $value ) :
				?>
				<div class="clearfix vi-header">
					<h2 class="vi-left-title pull-left">
						<a href="<?php echo get_term_link($value) ?>"><?php echo $value->name ?></a>
					</h2>
					<div class="vi-right-link pull-right">
						<a class="vi-more button secondary light" href="<?php echo get_term_link($value) ?>">Xem tất cả</a></div>
				</div>
				<?php
//				var_dump($value);
				echo do_shortcode('[gap][ux_products type="row" columns__sm="2" columns__md="3" cat="'.$value->term_id.'"][gap]');
				$description = get_term_field( 'description', $value );
				echo '<div class="term-description" style="margin-bottom: 30px;">'.wc_format_content($description).'</div>';
			endforeach;
		else :
			?>
			<?php if ( have_posts() ) : ?>

			<?php
			do_action( 'woocommerce_before_shop_loop' );
			?>

			<?php woocommerce_product_loop_start(); ?>

			<?php woocommerce_product_subcategories(); ?>

			<?php while ( have_posts() ) : the_post(); ?>

				<?php wc_get_template_part( 'content', 'product' ); ?>

			<?php endwhile; // end of the loop. ?>

			<?php woocommerce_product_loop_end(); ?>
			<?php
			do_action( 'flatsome_products_after' );
			/**
			 * woocommerce_after_main_content hook
			 *
			 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
			 */
			do_action( 'woocommerce_after_main_content' );
			?>
			<?php
			/**
			 * woocommerce_after_shop_loop hook
			 *
			 * @hooked woocommerce_pagination - 10
			 */
			do_action( 'woocommerce_after_shop_loop' );
			?>
			<?php do_action( 'woocommerce_archive_description' ); ?>
		<?php elseif ( !woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>

			<?php wc_get_template( 'loop/no-products-found.php' ); ?>

		<?php endif; ?>

		<?php endif; ?>
	</div>
</div>
