<?php
/**
 * Share template
 *
 * @author  Your Inspiration Themes
 * @package YITH WooCommerce Wishlist
 * @version 2.0.13
 */

if( !defined( 'YITH_WCWL' ) ) {
	exit;
} // Exit if accessed directly

$classes = get_flatsome_icon_class( flatsome_option( 'social_icons_style' ) );
$classes = $classes . ' tooltip';

?>
<div class="yith-wcwl-share social-icons share-icons share-row relative">
	<span class="share-icons-title"><?php echo $share_title ?></span>
	<?php if( $share_facebook_enabled ): ?>
<!--		<a target="_blank" class="facebook --><?php //echo $classes; ?><!--"-->
<!--		   href="https://www.facebook.com/sharer.php?s=100&amp;p%5Btitle%5D=--><?php //echo $share_link_title ?><!--&amp;p%5Burl%5D=--><?php //echo urlencode( $share_link_url ) ?><!--"-->
<!--		   title="--><?php //_e( 'Facebook', 'yith-woocommerce-wishlist' ) ?><!--"><i class="icon-facebook"></i></a>-->
		<div class="fb-like" data-href="<?php echo urlencode( $share_link_url ) ?>" data-layout="button" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
	<?php endif; ?>
	<?php if( $share_googleplus_enabled ): ?>
		<a target="_blank" class="google-plus <?php echo $classes; ?>" href="https://plus.google.com/share?url=<?php echo $share_link_url ?>&amp;title=<?php echo $share_link_title ?>" title="<?php _e( 'Google+', 'yith-woocommerce-wishlist' ) ?>" onclick='javascript:window.open(this.href, "", "menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600");return false;'><i class="icon-google-plus"></i></a>
	<?php endif; ?>

</div>